//<#!@READ-ONLY-SECTION-START@!#>
/*
 * \class cfm_vulcanoapp
 * \brief Intel(R) CoFluent(TM) Studio - Intel Corporation
 * \details Simulation model of VulcanoApp generated by C++ Generator for SystemC/TLM2
 */
#include "cfm_vulcanoapp.h"
using namespace std;
using namespace sc_core;
using namespace cf_dt;
using namespace cf_pk;
using namespace cf_core;

//<#!@READ-ONLY-SECTION-END@!#>
//Start of 'VulcanoApp definitions' algorithm generated code

//End of 'VulcanoApp definitions' algorithm generated code
//<#!@READ-ONLY-SECTION-START@!#>

/// \name constructor
//@{
cfm_vulcanoapp::cfm_vulcanoapp(sc_core::sc_module_name name) :
		cf_application(name), mq_changedQ("changedQ") {
	cf_application::init();

	Consumer = new cfm_consumer("Consumer");

	Producer_1 = new cfm_producer_1("Producer_1");

	// connections
	Consumer->p_mq_changedQ(mq_changedQ.p_target_socket);

	Producer_1->p_mq_changedQ(mq_changedQ.p_target_socket);

	//<#!@READ-ONLY-SECTION-END@!#>
	//Start of 'VulcanoApp constructor' algorithm generated code

	//End of 'VulcanoApp constructor' algorithm generated code
	//<#!@READ-ONLY-SECTION-START@!#>

	cf_application::elab_end();
}
//@}

/// \name destructor
//@{
cfm_vulcanoapp::~cfm_vulcanoapp(void) {
	//<#!@READ-ONLY-SECTION-END@!#>
	//Start of 'VulcanoApp destructor' algorithm generated code

	//End of 'VulcanoApp destructor' algorithm generated code
	//<#!@READ-ONLY-SECTION-START@!#>
	delete Consumer;
	delete Producer_1;
}
//@}

/// \name pre-elaboration section
//@{
void cfm_vulcanoapp::cb_before_elaboration(void) {
	//<#!@READ-ONLY-SECTION-END@!#>
	//Start of 'VulcanoApp pre elaboration' algorithm generated code

	//End of 'VulcanoApp pre elaboration' algorithm generated code
	//<#!@READ-ONLY-SECTION-START@!#>
}
//@}

/// \name post-elaboration section
//@{
void cfm_vulcanoapp::cb_end_of_elaboration(void) {
//<#!@READ-ONLY-SECTION-END@!#>
//Start of 'VulcanoApp post elaboration' algorithm generated code

//End of 'VulcanoApp post elaboration' algorithm generated code
//<#!@READ-ONLY-SECTION-START@!#>
}
//@}

/// \name post-simulation section
//@{
void cfm_vulcanoapp::cb_end_of_simulation(void) {
	//<#!@READ-ONLY-SECTION-END@!#>
	//Start of 'VulcanoApp post simulation' algorithm generated code

	//End of 'VulcanoApp post simulation' algorithm generated code
	//<#!@READ-ONLY-SECTION-START@!#>

}
//@}

/// \name initialize attributes
//@{
void cfm_vulcanoapp::cb_init_attributes() {

// initialize function attributes
	cfa_cycle_period.init(cf_expr_time(10, CF_NS));

// initialize relations attributes
	mq_changedQ.cfa_send_time.init(cf_expr_duration(1, CF_NS));
	mq_changedQ.cfa_receive_time.init(cf_expr_duration(1, CF_NS));
	mq_changedQ.cfa_queue_policy.init(CF_MQ_POLICY_FIFO_FINITE);
	mq_changedQ.cfa_queue_capacity.init((cf_nonzero_count) 1);
	mq_changedQ.cfa_concurrency.init((cf_nonzero_count) 1);
	mq_changedQ.cfa_send_threshold.init((cf_nonzero_count) 1);
	mq_changedQ.cfa_receive_threshold.init((cf_nonzero_count) 1);

	return;
}
//@}

/// \name initialize definitions
//@{
void cfm_vulcanoapp::cb_init_local_vars(void) {
	//<#!@READ-ONLY-SECTION-END@!#>
	//Start of 'VulcanoApp initializations' algorithm generated code

	//End of 'VulcanoApp initializations' algorithm generated code
	//<#!@READ-ONLY-SECTION-START@!#>
}
//@}


//<#!@READ-ONLY-SECTION-END@!#>