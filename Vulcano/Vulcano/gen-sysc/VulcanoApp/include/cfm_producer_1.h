//<#!@READ-ONLY-SECTION-START@!#>
/*!
 * \class cfm_producer_1
 * \brief Intel(R) CoFluent(TM) Studio - Intel Corporation
 * \details Simulation model of VulcanoApp generated by C++ Generator for SystemC/TLM2
 */

#ifndef COFS_MODEL_VULCANOAPP_PRODUCER_1
#define COFS_MODEL_VULCANOAPP_PRODUCER_1

#include "cfm_vulcanoapp_global.h"
#include "cfm_vulcanoapp_global_types.h"
#include "cofluent.h"

//<#!@READ-ONLY-SECTION-END@!#>
//Start of 'Producer_1 includes' algorithm generated code

//End of 'Producer_1 includes' algorithm generated code
//<#!@READ-ONLY-SECTION-START@!#>

///    \defgroup dxgProducer_1 Function Producer_1
//@{
///        \page dxpProducer_1
//@{
///    \brief Producer_1 function model.
class cfm_producer_1: public cf_core::cf_function {
public:
	SC_HAS_PROCESS (cfm_producer_1);

	/// ports typedef
	typedef cf_core::cf_mq_initiator_socket<cfm_producer_1,
			cf_core::cf_payload_int> p_mq_changedQ_t;

	/// constructor
	cfm_producer_1(sc_core::sc_module_name name);

	/// destructor
	virtual ~cfm_producer_1(void);
	virtual void cb_before_elaboration(void);

public:
	/// \name input/output ports
	//@{
	p_mq_changedQ_t p_mq_changedQ;
	//@}

protected:
	/// initialize attributes
	void cb_init_attributes(void);
	/// initialize definitions
	void cb_init_local_vars(void);

	/// behavior
	void cfm_behavior();
	/// init operation
	void cfm_op_init();
	/// produce operation
	void cfm_op_produce();

protected:
	/// \name operations
	//@{
	cf_core::cf_operation init;
	cf_core::cf_operation produce;
	//@}

private:
	/// \name input/output local buffers
	//@{
	int changedQ;
	cf_core::cf_payload_int changedQ_trans;
	//@}

	/// \name user-defined local declarations
	//<#!@READ-ONLY-SECTION-END@!#>
	//Start of 'Producer_1 local declarations' algorithm generated code

	//End of 'Producer_1 local declarations' algorithm generated code
	//<#!@READ-ONLY-SECTION-START@!#>

};

//@}
//@}
#endif // COFS_MODEL_VULCANOAPP_PRODUCER_1

//<#!@READ-ONLY-SECTION-END@!#>